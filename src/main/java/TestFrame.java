import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestFrame {
	
	@Test
	public void deveInteragirComFrames() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		driver.switchTo().frame("frame1");
		driver.findElement(By.id("frameButton")).click();
		Alert alert = driver.switchTo().alert();
		String texto = alert.getText();
		Assert.assertEquals("Frame OK!", texto);
		alert.accept();
		driver.switchTo().defaultContent();
		
		driver.findElement(By.id("ElementosForm:nome")).sendKeys(texto);
		
		driver.quit();
		
	}
	

}
