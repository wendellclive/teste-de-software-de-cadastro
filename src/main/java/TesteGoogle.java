import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteGoogle {
	
	@Test
	public void teste() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
//		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/chromedriver/chromedriver");
		
//		WebDriver driver = new FirefoxDriver();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://www.google.com");
		System.out.println(driver.getTitle());
		
		Assert.assertEquals("Google", driver.getTitle());
		
		driver.quit();
		
	}

}
