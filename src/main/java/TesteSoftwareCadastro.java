import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


public class TesteSoftwareCadastro {

	@Test
	public void testeTextField() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		driver.findElement(By.id("elementosForm:nome")).sendKeys("Teste de Escrita");
		
		Assert.assertEquals("Teste de Escrita", driver.findElement(By.id("elementosForm:nome")).getAttribute("value"));
		
		driver.quit();
		
	}
	
	@Test
	public void deveInteragirComTextArea() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		driver.findElement(By.id("elementosForm:sugestoes")).sendKeys("Teste");
		
		Assert.assertEquals("Teste", driver.findElement(By.id("elementosForm:sugestoes")).getAttribute("value"));
		
		driver.quit();
		
	}
	
	
	@Test
	public void deveInteragirComRadioButton() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		driver.findElement(By.id("elementosForm:sexo:0")).click();
		driver.findElement(By.id("elementosForm:sexo:0")).isSelected();
		
		Assert.assertTrue((driver.findElement(By.id("elementosForm:sexo:0")).isSelected()));
		
		driver.quit();
		
	}
	
	@Test
	public void deveInteragirComCheckBox() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		driver.findElement(By.id("elementosForm:comidaFavorita:0")).click();
		driver.findElement(By.id("elementosForm:comidaFavorita:0")).isSelected();
		
		Assert.assertTrue((driver.findElement(By.id("elementosForm:comidaFavorita:0")).isSelected()));
		
		driver.quit();
		
	}

	@Test
	public void deveInteragirComboBox() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select combo = new Select(element);
		
		combo.selectByIndex(1);
	
		Assert.assertEquals("1o grau completo", combo.getFirstSelectedOption().getText());
		driver.quit();
		
	}

	@Test
	public void deveVerificarValoresCombo() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select combo = new Select(element);
		List<WebElement> options = combo.getOptions();
		
		Assert.assertEquals(8, options.size());
		
		boolean encontrou = false;
		
		for (WebElement option : options) {
			if(option.getText().equals("Mestrado")) {
				encontrou = true;
				break;
			}
			
		}
		
		Assert.assertTrue(encontrou);
		driver.quit();
		
	}
	
	@Test
	public void deveVerificarValoresComboMultiplo() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		WebElement element = driver.findElement(By.id("elementosForm:esportes"));
		Select combo = new Select(element);
		
		combo.selectByVisibleText("Natacao");
		combo.selectByVisibleText("Corrida");
		combo.selectByVisibleText("O que eh esporte?");
		
		List<WebElement> allSelecElements = combo.getAllSelectedOptions();
		Assert.assertEquals(3, allSelecElements.size());
		
		combo.deselectByVisibleText("Corrida");

		List<WebElement> allSelecElements2 = combo.getAllSelectedOptions();
		Assert.assertEquals(2, allSelecElements2.size());

		driver.quit();
		
	}
	
	@Test
	public void deveInteragirComBotoes() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		WebElement botao = driver.findElement(By.id("buttonSimple"));
		botao.click();
		
		Assert.assertEquals("Obrigado!", botao.getAttribute("value"));
		driver.quit();
		
	}
	
	@Test
	@Ignore
	public void deveInteragirComLinks() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		driver.findElement(By.linkText("Voltar")).click();
		
		Assert.assertEquals("Voltou!", 	driver.findElement(By.id("resiltado")).getText());
	
		driver.quit();

	}
	
	@Test
	public void deveBuscarTextosNaPagina() {
		
		System.setProperty("webdriver.gecko.driver", "/home/wendell/Documentos/drivers-selenium/geckodriver/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
//		driver.findElement(By.tagName("body")).getText();
//		Assert.assertTrue(driver.findElement(By.tagName("body")).getText().contains("Campo de Treinamento"));
		
		Assert.assertEquals("Campo de Treinamento", driver.findElement(By.tagName("h3")).getText());
		Assert.assertEquals("Cuidado onde clica, muitas armadilhas...", driver.findElement(By.className("facilAchar")).getText());
	
		driver.quit();

	}
	
}
